import Image from 'next/image'
import styles from '../styles/Home.module.css'

const Footer = () => (
    <footer className={styles.footer}>
        <a
            href="https://ktc.agency"
            target="_blank"
            rel="noopener noreferrer"
        >
            Powered by{' '}
            <span className={styles.logo}>
            <Image src="/ktc_logo.jpg" alt="ktc Logo" width={32} height={32} />
            </span>
        </a>
    </footer>
)

export default Footer